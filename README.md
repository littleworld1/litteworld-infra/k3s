# K3s

## Install K3s
```
mkdir ~/.kube && \
curl -sfL https://get.k3s.io | sh -s - \
  --tls-san 132.226.211.103 \
  --tls-san c4m24.little.world.oraclevcn.com \
  --node-external-ip 132.226.211.103 \
  --advertise-address 132.226.211.103 \
  --write-kubeconfig ~/.kube/config \
  --disable servicelb \
  --disable traefik \
  --kube-apiserver-arg service-node-port-range=80-6442
```

Error `invalid capacity 0 on image filesystem`
```
mkdir -p /boot/firmware && \
echo "group_enable=memory cgroup_memory=1" >> /boot/firmware/cmdline.txt
```

## Install K9s for ARM64
```
export K9S_VER=0.24.14 && \
wget https://github.com/derailed/k9s/releases/download/v${K9S_VER}/k9s_Linux_arm64.tar.gz && \
tar -xzf k9s_Linux_arm64.tar.gz && \
mv -f k9s /usr/local/sbin/ && \
rm -f k9s_*.gz
```

## Install K9s for AMD64
```
export K9S_VER=0.24.14 && \
wget https://github.com/derailed/k9s/releases/download/v${K9S_VER}/k9s_Linux_x86_64.tar.gz && \
tar -xzf k9s_Linux_x86_64.tar.gz && \
mv -f k9s /usr/local/bin/ && \
rm -f k9s_*.gz
```
